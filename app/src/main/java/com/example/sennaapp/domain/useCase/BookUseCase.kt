package com.example.sennaapp.domain.useCase


import com.example.sennaapp.data.entitiy.BooksModel
import com.example.sennaapp.domain.IBooksRepository
import retrofit2.Response


suspend fun showAllBooks(
    bookRepo: IBooksRepository
): Response<BooksModel>? =
    bookRepo.getBooks()


