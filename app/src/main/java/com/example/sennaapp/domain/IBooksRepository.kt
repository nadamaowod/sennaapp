package com.example.sennaapp.domain

import com.example.sennaapp.data.entitiy.BooksModel
import retrofit2.Response


interface IBooksRepository {

    suspend fun getBooks(): Response<BooksModel>?
}

interface IBooksDataSource {

    suspend fun getBooks(): Response<BooksModel>?
}