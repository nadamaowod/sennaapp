package com.example.sennaapp.data.di


import com.example.sennaapp.data.source.BooksRemoteDataSource
import com.example.sennaapp.data.source.DefaultRepo
import com.example.sennaapp.data.soure.remote.ApiInterface
import com.example.sennaapp.domain.IBooksDataSource
import com.example.sennaapp.domain.IBooksRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModul {

    @Singleton
    @Provides
    fun provideRemoteProductsDataSource(
        api: ApiInterface
    ): IBooksDataSource = BooksRemoteDataSource(api)
/*

    @Singleton
    @Provides
    fun provideDataSource(
        remote: IBooksDataSource):IBooksRepository  = DefaultRepo(remote ) as IBooksRepository
*/

    @Singleton
    @Provides
    fun provideDefaultProductRepository(
        remote: IBooksDataSource):IBooksRepository  = DefaultRepo(remote ) as IBooksRepository

}