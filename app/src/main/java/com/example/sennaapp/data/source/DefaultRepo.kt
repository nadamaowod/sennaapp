package com.example.sennaapp.data.source

import com.example.sennaapp.domain.IBooksDataSource
import com.example.sennaapp.domain.IBooksRepository
import retrofit2.Response
import javax.inject.Inject


class DefaultRepo @Inject constructor(
    private val booksRemoteDataSource: IBooksDataSource,
    ) : IBooksRepository {

    override suspend fun getBooks()=  booksRemoteDataSource.getBooks()

}





