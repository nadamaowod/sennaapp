package com.example.sennaapp.data.source

import android.util.Log
import com.example.sennaapp.data.entitiy.BooksModel
import com.example.sennaapp.data.soure.remote.ApiInterface
import com.example.sennaapp.domain.IBooksDataSource
import com.example.sennaapp.util.Constants

import retrofit2.Response
import javax.inject.Inject


class BooksRemoteDataSource @Inject constructor(
    private val apiInterface: ApiInterface
) : IBooksDataSource {

    override suspend fun getBooks(): Response<BooksModel> ? {
        try {
            return apiInterface.getBooks(Constants.API_KEY)
        } catch (e: Exception) {
            Log.d("BOOK ", e.message.toString())
            return null
        }
    }


}





