package com.example.sennaapp.data.soure.remote


import com.example.sennaapp.data.entitiy.BooksModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {


    @GET("lists/current/hardcover-fiction.json")
    suspend fun getBooks(
        @Query("api-key") apiKey: String): Response<BooksModel>

}
