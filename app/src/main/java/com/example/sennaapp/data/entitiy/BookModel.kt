package com.example.sennaapp.data.entitiy

import android.os.Parcel
import android.os.Parcelable

data class BooksModel(

    val status: String,
    val copyright: String,
    val num_results: Int,
    val last_modified: String,
    val results: Result
)

data class Result(

    val list_name: String,
    val list_name_encoded: String,
    val bestsellers_date: String,
    val published_date: String,
    val published_date_description: String,
    val next_published_date: String,
    val previous_published_date: String,
    val display_name: String,
    val normal_list_ends_at: Int,
    val updated: String,
    val books: List<BookData>,
    val corrections: List<String>
)

data class BookData(
    val rank: Int,
    val rank_last_week: Int,
    val weeks_on_list: Int,
    val asterisk: Int,
    val dagger: Int,

    val publisher: String,
    val description: String,
    val price: Double,
    val title: String,
    val author: String,
    val contributor: String,
    val contributor_note: String,
    val book_image: String,
    val book_image_width: Int,
    val book_image_height: Int,
    val amazon_product_url: String,
    val age_group: String,
    val book_review_link: String,
    val first_chapter_link: String,
    val sunday_review_link: String,
    val article_chapter_link: String,
    //  val isbns: List<Isbns>,
    //  val buy_links: List<Buy_links>,
    val book_uri: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readDouble(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString()
    ) {
    }

    override fun describeContents(): Int {
        return 0
    }


    override fun writeToParcel(p0: Parcel?, p1: Int) {
    }

    companion object CREATOR : Parcelable.Creator<BookData> {
        override fun createFromParcel(parcel: Parcel): BookData {
            return BookData(parcel)
        }

        override fun newArray(size: Int): Array<BookData?> {
            return arrayOfNulls(size)
        }
    }
}

data class Buy_links(

    val name: String,
    val url: String
)


data class Isbns(

    val isbn10: Long,
    val isbn13: Long
)