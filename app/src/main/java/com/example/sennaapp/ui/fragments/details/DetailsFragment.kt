package com.example.sennaapp.ui.fragments.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.sennaapp.R
import com.example.sennaapp.data.entitiy.BookData
import com.example.sennaapp.databinding.FragmentDetailsBinding
import com.example.sennaapp.util.Constants
import com.example.sennaapp.util.Extension.capitalize
import com.example.sennaapp.util.Extension.loadImage
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {

    private lateinit var bookData: BookData
    private lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_details,
            container,
            false
        )
        init()

        return binding.root
    }


    private fun init() {
        bookData = arguments?.getParcelable<BookData>(Constants.BOOK_KEY)!!
        with(binding) {
            imgBook.loadImage(bookData.book_image, requireActivity())

            txttitle.text = capitalize(bookData.title.lowercase())
            txtPublishedBy.text = bookData.publisher
            txtRating.text = bookData.rank.toString()
            txtSummery.text = bookData.description

            imgBook.setOnClickListener(View.OnClickListener { view ->

                val bundle = bundleOf(Constants.BOOK_IMAGE_KEY to bookData.book_image)
                findNavController().navigate(R.id.action_detailFragment_to_fullScreenFragment,bundle)
            })
        }
    }
}