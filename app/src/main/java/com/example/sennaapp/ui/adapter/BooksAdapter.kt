package com.example.sennaapp.ui.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sennaapp.data.entitiy.BookData
import com.example.sennaapp.databinding.RowBookBinding
import com.example.sennaapp.ui.interfaces.BooksItemClickListener
import com.example.sennaapp.util.Extension.capitalize
import com.example.sennaapp.util.Extension.loadImage

class BooksAdapter constructor(val onclick: BooksItemClickListener, val context: Context) :
    RecyclerView.Adapter<BooksAdapter.ViewHolder>() {

    private var bookList: ArrayList<BookData> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val li = LayoutInflater.from(context)
        return ViewHolder(
            RowBookBinding.inflate(li), onclick
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(bookList.get(position))

    inner class ViewHolder(
        private val binding: RowBookBinding,
        onclick: BooksItemClickListener,
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        fun bind(book: BookData) = with(binding) {
            item = book
          txtTitle.text=capitalize(book.title.lowercase())

            imgBook.loadImage(book.book_image, context)
            lytParent.setOnClickListener(this@ViewHolder)
            executePendingBindings()
        }

        override fun onClick(p0: View?) {
            if (p0 != null) {
                onclick.view(p0,layoutPosition)
            }
        }
    }

    override fun getItemCount(): Int {
        return bookList.size
    }


    fun getBookList(): ArrayList<BookData> {
        return bookList
    }

    fun setList(list: List<BookData>) {
        this.bookList = list as ArrayList<BookData>
        this.notifyDataSetChanged()
    }

}


