package com.example.sennaapp.ui.interfaces

import android.view.View

interface BooksItemClickListener {
    fun view(view: View, itemId: Int)

}