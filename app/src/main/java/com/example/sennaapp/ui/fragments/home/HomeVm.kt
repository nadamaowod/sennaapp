package com.example.sennaapp.ui.fragments.home

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sennaapp.data.entitiy.BookData
import com.example.sennaapp.data.entitiy.BooksModel
import com.example.sennaapp.domain.IBooksRepository
import com.example.sennaapp.domain.useCase.showAllBooks
import com.example.sennaapp.util.NetworkConnectivity
import com.example.sennaapp.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class HomeVm @Inject  internal constructor(
    var iBooksRepository: IBooksRepository
) : ViewModel() {
    private lateinit var context: Context

    var booksList: MutableLiveData<Resource<List<BookData>>> = MutableLiveData()
    var booksResponse: List<BookData>? = null


    fun setActivity(context: Context) {
        this.context = context

    }


  fun getBooks(): MutableLiveData<Resource<List<BookData>>> {
        //   productsList.postValue(Resource.Loading())
        try {
            if (NetworkConnectivity.hasInternetConnection(context)) {
                viewModelScope.launch {
                    val response = showAllBooks(iBooksRepository )
                    response?.let { handleBannerResponse(it) }?.let { booksList.postValue(it) }
                }
            } else {
                booksList.postValue(Resource.Error("No internet connection"))

            }

        } catch (e: Exception) {
            when (e) {
                is IOException -> booksList.postValue(Resource.Error("Network Failure"))
                else -> booksList.postValue(Resource.Error("Conversion Error"))
            }
        }


        return booksList
    }

    private fun handleBannerResponse(response: Response<BooksModel>): Resource<List<BookData>> {
        if (response.isSuccessful) {
            response.body().let { it ->
                if (booksResponse == null) {
                    if (it != null) {
                        booksResponse = it.results.books
                    }
                }
                return booksResponse?.let { it1 -> Resource.Success(it1) }!!
            }

        } else return Resource.Error(response.message())

    }



}