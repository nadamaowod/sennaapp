package com.example.sennaapp.ui.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.sennaapp.R
import com.example.sennaapp.databinding.FragmentHomeBinding
import com.example.sennaapp.ui.adapter.BooksAdapter
import com.example.sennaapp.ui.interfaces.BooksItemClickListener
import com.example.sennaapp.util.Constants
import com.example.sennaapp.util.Extension.hideProgressBar
import com.example.sennaapp.util.Extension.init
import com.example.sennaapp.util.Extension.showProgressBar
import com.example.sennaapp.util.Extension.withFragment
import com.example.sennaapp.util.Resource
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeFragment : Fragment(), BooksItemClickListener {

    private lateinit var binding: FragmentHomeBinding
    private val homeVm: HomeVm by viewModels()
    private lateinit var adapter: BooksAdapter
    var isLoading = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        setVm()
        return binding.root
    }

    private fun setVm() {
        with(binding) {
            withFragment(this)
            binding.lifecycleOwner = this.lifecycleOwner
            activity?.let { homeVm.setActivity(it) }
        }
        initViews()
        getData();
    }

    private fun getData() {

        homeVm.getBooks()
        homeVm.booksList.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar(binding.progressBar)
                    isLoading = false
                    response.data?.let { newsResponse ->
                        adapter.setList(response.data)
                    }
                }
                is Resource.Error -> {
                    hideProgressBar(binding.progressBar)
                    isLoading = false

                    response.message?.let { message ->
                        Toast.makeText(activity, "An error occured: $message", Toast.LENGTH_LONG)
                            .show()
                    }
                }
                is Resource.Loading -> {
                    showProgressBar(binding.progressBar)
                    isLoading = true
                }
            }
        })
    }

    private fun initViews() {
        adapter = activity?.let { BooksAdapter(this, it) }!!
        binding.RecHomeBooks.init(requireContext(), adapter)

    }

    override fun view(view: View, itemId: Int) {
        var book = adapter.getBookList()[itemId]!!

        val bundle = bundleOf(Constants.BOOK_KEY to book)

        findNavController().navigate(R.id.action_homeFragment_to_detailFragment,bundle)

    }

}