package com.example.sennaapp.ui.fragments.fullScreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.sennaapp.R
import com.example.sennaapp.databinding.FragmentFullScreenBinding
import com.example.sennaapp.util.Constants
import com.example.sennaapp.util.Extension.loadImage


class FullScreenFragment : Fragment() {

    private lateinit var binding: FragmentFullScreenBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_full_screen,
            container,
            false
        )
        var img = arguments?.getString(Constants.BOOK_IMAGE_KEY)
        if (img != null) {
             binding.imgZoom.loadImage(img, requireActivity())
        }
        return binding.root
    }
}